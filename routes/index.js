var express = require("express");
var router = express.Router();

require("dotenv").config();
const API_KEY = process.env.API_KEY;
const LAT = process.env.LAT ? process.env.LAT : "37.7700655";
const LON = process.env.LON ? process.env.LON : "-1.4990814";
const EXCLUDE = process.env.EXCLUDE ? process.env.EXCLUDE : "hourly,minutely";
const CACHE_TIME = process.env.CACHE_TIME ? process.env.CACHE_TIME : 60000;

var axios = require("axios");
const cache = require("memory-cache");

const cacheKey = "weather";

router.get(
  "/weather/",

  function (req, res, next) {
    const url = `http://api.openweathermap.org/data/2.5/onecall?lat=${LAT}&lon=${LON}&exclude=${EXCLUDE}&units=metric&lang=es&appid=${API_KEY}`;

    let mcache = cache.get(cacheKey);
    if (mcache) {
      res.send(mcache);
    } else {
      axios
        .get(url)
        .then((response) => {
          cache.put(cacheKey, response.data, CACHE_TIME);
          res.send(response.data);
          return;
        })
        .catch(function (error) {
          // handle error
          console.error(error);
        });
    }
  }
);

router.get("/clearCache/", function (req, res, next) {
  cache.clear();
  res.send("success");
});

module.exports = router;
